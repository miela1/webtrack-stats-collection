﻿using Dapper;
using System.Data.SqlClient;
using WebtrackFeatures.Core.DataItems;

namespace WebtrackFeatures.Core
{
	public static class CustomerFactory
	{
		public static Count GetCustomersCount()
		{
			const string sql = @"
								DECLARE @T TABLE ( Total INT, Active INT );
								DECLARE @Total INT = (SELECT COUNT(ID) FROM Customers); 
								DECLARE @Active INT = (SELECT COUNT(ID) AS Total FROM Customers WHERE IsDeleted != 0)
								  
								INSERT INTO @T (Total, Active) VALUES (@Total, @Active)
								SELECT Total, Active FROM @T";

			return new SqlConnection(DA.GetConnectionString(DatabaseNames.web)).QuerySingle<Count>(sql);
		}
	}
}
