﻿using Dapper;
using System.Data.SqlClient;
using WebtrackFeatures.Core.DataItems;

namespace WebtrackFeatures.Core
{
	public static class UserFactory
	{
		public static Count GetUserCount()
		{
			const string sql = @"
						DECLARE @T TABLE ( Total INT, Active INT );
						DECLARE @Total INT = (SELECT COUNT(ID) FROM Users); 
						DECLARE @Active INT = (SELECT COUNT(ID) AS Total FROM Users WHERE IsDeleted != 0)
						INSERT INTO @T (Total, Active) VALUES (@Total, @Active)
						SELECT Total, Active FROM @T";

			var result = DA.Connections.Shared.QuerySingle<Count>(sql);
			//var result = new SqlConnection(DA.GetConnectionString(DatabaseNames.shared)).QuerySingle<Count>(sql);
			return result;
		}
	}
}
