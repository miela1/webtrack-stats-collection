﻿using Dapper;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using r2pCore.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using WebtrackFeatures.Core.DataItems;

namespace WebtrackFeatures.Core
{
	public static class StatsFactory
	{
		public static List<ActivityLogDbData> GetActivityLogs(IMemoryCache cache)
		{
			var cacheEntry = cache.GetOrCreate("activityLog_cache_key", entry =>
			{
				entry.SetSlidingExpiration(TimeSpan.FromMinutes(5));
				entry.SetAbsoluteExpiration(TimeSpan.FromMinutes(20));

				var toDate = DateTime.Now;
				var fromDate = toDate.AddMonths(-1);
				return GetActivityLogs(fromDate, toDate);
			});

			return cacheEntry;
		}
		public static List<ActivityLogDbData> GetActivityLogs(DateTime from, DateTime to)
		{
			const string sql = @"SELECT CustomerId, UserId, [TimeStamp], [Url], [Action], MessagePackData
								 FROM ActivityLog
								 WHERE TimeStamp BETWEEN @DateFrom AND @DateTo
								 ORDER BY TimeStamp DESC";

			var data = new SqlConnection(DA.GetConnectionString(DatabaseNames.wsc)).Query<ActivityLogDbData>(sql, new { DateFrom = from, DateTo = to }).ToList();

			return data;
		}


		public static List<FunctionRefrence> GetFunctionNames(IMemoryCache cache)
		{
			var cacheEntry = cache.GetOrCreate("function_cache_key", entry =>
			{
				entry.SetSlidingExpiration(TimeSpan.FromMinutes(5));
				entry.SetAbsoluteExpiration(TimeSpan.FromMinutes(20));

				var result = new Dictionary<string, string>();
				const string sql = "SELECT Name, Selector, Page FROM ActivityLogFunctionRefrence";

				var data = new SqlConnection(DA.GetConnectionString(DatabaseNames.wsc)).Query<FunctionRefrence>(sql).ToList();

				return data;
			});

			return cacheEntry;
		}




		[Obsolete("GetSystems is deprecated, please use GetActivityLogs instead.")]
		public static List<UserSystem> GetSystems()
		{
			const string sql = "SELECT JsonData FROM ActivityLog WHERE TimeStamp BETWEEN DATEADD(MONTH, -1,GETDATE()) AND GETDATE() AND [Action] = 'PAGELOAD' AND Url like '%web-track.dk/'";

			var rawData = new SqlConnection(DA.GetConnectionString(DatabaseNames.wsc)).Query<string>(sql).ToList();
			var result = new List<UserSystem>();

			foreach (var item in rawData)
			{
				var json = JsonConvert.DeserializeObject<JsonActivityData>(item);
				result.Add(new UserSystem
				{
					Darkmode = json.BrowserInfo.Darkmode,
					Language = json.BrowserInfo.Language,
					UAString = json.BrowserInfo.UserAgent,
					BrowserVersion = "",//$"{info.UA.Major}.{info.UA.Minor}.{info.UA.Patch}",
					OsVersion = "",//$"{info.OS.Major}.{info.OS.Minor}",
					Browser = "",//info.UA.Family,
					Os = "",//info.OS.Family,
					DeviceType = "",//info.Device.Family == "Other" ? "PC/MAC" : "Mobile/Tablet"
				});
			}
			return result;
		}
		[Obsolete("GetDayVisits is deprecated, please use GetActivityLogs instead.")]
		public static int GetDayVisits()
		{
			const string sql = "SELECT Count(0) FROM ActivityLog WHERE TimeStamp BETWEEN DATEADD(DAY, -1,GETDATE()) AND GETDATE() AND [Action] = 'PAGELOAD' AND Url like '%web-track.dk/'";
			return DA.Connections.WSC.QuerySingle<int>(sql);
		}

		[Obsolete("GetLoadsAndLeaves is deprecated, please use GetActivityLogs instead.")]
		public static Dictionary<int, List<LoadsAndLeaves>> GetLoadsAndLeaves()
		{
			const string sql = @"SELECT UserId, Timestamp, Url, [Action] FROM ActivityLog 
								WHERE TimeStamp BETWEEN DATEADD(MONTH, -1, GETDATE()) 
								AND GETDATE() 
								AND [Action] IN ('PAGELOAD','PAGELEAVE')
								AND Url like '%web-track.dk/%'
								GROUP BY Url, UserId, TimeStamp, Action
								ORDER BY Timestamp";

			var tmp = new SqlConnection(DA.GetConnectionString(DatabaseNames.wsc)).Query<LoadsAndLeaves>(sql).ToList();
			var result = new Dictionary<int, List<LoadsAndLeaves>>();

			foreach (var i in tmp)
			{
				i.Url = i.Url.Split('?')[0];
				i.Url = i.Url.Replace('#', ' ');
				i.Url = i.Url.Trim();
				if (!result.ContainsKey(i.UserId))
					result.Add(i.UserId, new List<LoadsAndLeaves>());
				result[i.UserId].Add(i);
			}

			return result;
		}

		[Obsolete("GetClickPositions is deprecated, please use GetActivityLogs instead.")]
		public static List<Position> GetClickPositions(string page)
		{
			const string sql = "SELECT JsonData FROM ActivityLog WHERE [ACTION] = 'MOUSECLICK'  AND TimeStamp BETWEEN DATEADD(MONTH, -1, GETDATE()) AND GETDATE() AND Url LIKE '%web-track.dk/' + @Page + '%'";

			var data = new SqlConnection(DA.GetConnectionString(DatabaseNames.wsc)).Query<string>(sql, new { Page = page }).ToList();
			var result = new List<Position>();

			foreach (var item in data)
			{
				var json = JObject.Parse(item);
				var mouse = json["MousePosition"];
				var size = json["WindowSize"];

				if (mouse != null && size != null)
				{
					var mouseX = int.Parse(mouse["x"].ToString());
					var mouseY = int.Parse(mouse["y"].ToString());
					if (mouseX == 0 && mouseY == 0)
						continue;

					var tmp = new Position
					{
						X = mouseX,
						Y = mouseY,
						Width = int.Parse(size["width"].ToString()),
						Height = int.Parse(size["height"].ToString())
					};

					result.Add(tmp);
				}
			}

			return result;
		}
		[Obsolete("GetFunctionCount is deprecated, please use GetActivityLogs instead.")]
		public static Dictionary<string, int> GetFunctionCount(string page)
		{
			const string sql = "SELECT JsonData FROM ActivityLog WHERE [ACTION] = 'MOUSECLICK'  AND TimeStamp BETWEEN DATEADD(MONTH, -1, GETDATE()) AND GETDATE() AND Url LIKE '%web-track.dk/' + @Page + '%'";

			var dataStartTime = DateTime.Now;
			var data = new SqlConnection(DA.GetConnectionString(DatabaseNames.wsc)).Query<string>(sql, new { Page = page }).ToList();
			var totalDataTime = (DateTime.Now - dataStartTime).TotalMilliseconds.ToInt();



			var result = new Dictionary<string, int>();
			foreach (var item in data)
			{
				if (item.Contains("TargetSelect"))
				{
					var tragetSelect = JObject.Parse(item)["TargetSelect"].ToString();
					if (Statics.FunctionRefrence.TryGetValue(tragetSelect, out string name))
					{
						if (!result.ContainsKey(name))
							result.Add(name, 0);
						result[name] = result[name] + 1;
					}
				}
			}
			return result;

		}
		public static Dictionary<string, string> GetFunctionRefrence()
		{
			const string sql = "SELECT Name, Selector FROM ActivityLogFunctionRefrence";

			var data = new SqlConnection(DA.GetConnectionString(DatabaseNames.wsc)).Query<FunctionRefrence>(sql);
			return data.ToDictionary(k => k.Selector, s => s.Name);
		}
	}
}
