﻿using r2pCore.Common.Extensions;
using System.Data;
using System.Data.SqlClient;

namespace WebtrackFeatures.Core
{
	/// <summary>
	/// Gets the defined supported databases
	/// </summary>
	public enum SupportedDatabases
	{
		/// <summary>
		///
		/// </summary>
		MSSQL = 0
	}

	/// <summary>
	/// Gets the defined supported database names to get connections to
	/// </summary>
	public enum DatabaseNames
	{
		/// <summary>
		/// Connection to WebTrack
		/// </summary>
		web = 0,
		wsc = 1,
		shared = 2

	}

	static class DA
	{
		/// <summary>
		/// Gets the current active state of DAG
		/// </summary>
		public static string DagState
		{
			get
			{
				var val = Configs.DBConnections.DagState;
				if (!val.HasValue())
					return "debug";
				return val.ToLowerInvariant();
			}
		}

		/// <summary>
		/// Gets a connection string for a specific database
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public static string GetConnectionString(DatabaseNames name)
		{
			var fullName = $"{name.ToLowerInvariantString()}_{DagState}";
			return Configs.DBConnections.ConnectionStrings[fullName];
		}

		/// <summary>
		/// Converts a DatabaseName to a connection
		/// </summary>
		/// <param name="dbName">DatabaseName to convert</param>
		/// <returns></returns>
		public static IDbConnection ConvertDbToConnection(DatabaseNames dbName)
		{
			switch (dbName)
			{
				case DatabaseNames.web:
					return GetActiveConnection(SupportedDatabases.MSSQL, dbName);
				case DatabaseNames.wsc:
					return GetActiveConnection(SupportedDatabases.MSSQL, dbName);
				case DatabaseNames.shared:
					return GetActiveConnection(SupportedDatabases.MSSQL, dbName);
			}
			return null;
		}

		/// <summary>
		/// Gets the correct connection for the database
		/// </summary>
		/// <param name="supportedDb">Connection to get</param>
		/// <param name="dbName">Database to get connection for</param>
		/// <returns></returns>
		static IDbConnection GetActiveConnection(SupportedDatabases supportedDb, DatabaseNames dbName)
		{
			switch (supportedDb)
			{
				case SupportedDatabases.MSSQL:
					return new SqlConnection(GetConnectionString(dbName));
			}
			return null;
		}

		/// <summary>
		/// Gets a connection to a database name
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public static IDbConnection GetConnection(DatabaseNames name)
		{
			return ConvertDbToConnection(name);
		}

		/// <summary>
		/// Quick access a connection to a database
		/// </summary>
		public static StaticConnections Connections { get; } = new StaticConnections();


		/// <summary>
		/// Static connections to a database
		/// </summary>
		public class StaticConnections
		{
			/// <summary>
			/// Return a connection to: Web
			/// </summary>
			public IDbConnection Web { get; } = GetConnection(DatabaseNames.web);
			public IDbConnection WSC { get; } = GetConnection(DatabaseNames.wsc);
			public IDbConnection Shared { get; } = GetConnection(DatabaseNames.shared);

		}
	}
}
