﻿using System.Collections.Generic;
using System.Text;

namespace WebtrackFeatures.Core.DataItems
{
	public class UserSystem
	{
		public string Os { get; set; }
		public string Browser { get; set; }
		public string OsVersion { get; set; }
		public string BrowserVersion { get; set; }
		public string Language { get; set; }
		public string DeviceType { get; set; }
		public string UAString { get; set; }
		public bool Darkmode { get; set; }
	}
}
