﻿using System;

namespace WebtrackFeatures.Core.DataItems
{
	public class Position
	{
		public int X { get; set; }
		public int Y { get; set; }

		public int Width { get; set; }
		public int Height { get; set; }

		public override bool Equals(object obj)
		{
			return obj is Position position &&
				   X == position.X &&
				   Y == position.Y;
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(X, Y, Width, Height);
		}
	}
}
