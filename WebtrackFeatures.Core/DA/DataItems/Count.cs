﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebtrackFeatures.Core.DataItems
{
	public class Count
	{
		public int Total { get; set; }
		public int Active { get; set; }
	}
}
