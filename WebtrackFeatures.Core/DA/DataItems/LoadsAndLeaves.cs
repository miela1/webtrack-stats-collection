﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebtrackFeatures.Core.DataItems
{
	public class LoadsAndLeaves
	{
		public int UserId { get; set; }
		public DateTime Timestamp { get; set; }
		public string Url { get; set; }
		public string Action { get; set; }
	}
}
