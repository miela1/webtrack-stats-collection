﻿using System;

namespace WebtrackFeatures.Core.DataItems
{
	public class JsonActivityData
	{
		public string Action { get; set; }
		public string Url { get; set; }
		public DateTime Timestamp { get; set; }
		public BrowserInfomation BrowserInfo { get; set; }

		public Position MousePosition { get; set; }
		public Size WindowSize { get; set; }
	}
	public class BrowserInfomation
	{
		public string Language { get; set; }
		public string UserAgent { get; set; }
		public bool Darkmode { get; set; }
	}

	public class Size
	{
		public int Width { get; set; }
		public int Height { get; set; }
	}
}


