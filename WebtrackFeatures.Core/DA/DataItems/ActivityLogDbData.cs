﻿
using System;

namespace WebtrackFeatures.Core.DataItems
{
	public class ActivityLogDbData
	{
		public int CustomerId { get; set; }
		public int UserId { get; set; }

		public string Url { get; set; }
		public string Action { get; set; }

		public byte[] MessagePackData { get; set; }

		public DateTime Timestamp { get; set; }
	}
}
