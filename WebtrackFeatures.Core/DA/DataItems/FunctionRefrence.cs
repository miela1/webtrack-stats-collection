﻿namespace WebtrackFeatures.Core.DataItems
{
	public class FunctionRefrence
	{
		public string Name { get; set; }
		public string Selector { get; set; }
		public string Page { get; set; }
	}
}
