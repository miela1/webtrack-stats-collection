﻿using Dapper;
using System.Data.SqlClient;
using WebtrackFeatures.Core.DataItems;

namespace WebtrackFeatures.Core
{
	public static class DevciceFactory
	{
		public static Count GetDeviceCount()
		{
			const string sql = @"DECLARE @T TABLE ( Total INT, Active INT );
							     DECLARE @Total INT = (SELECT COUNT(ID) FROM CustomerDevices); 
								 DECLARE @Active INT = (SELECT COUNT(ID) AS Total FROM CustomerDevices WHERE IsDeleted != 0)
  
								 INSERT INTO @T (Total, Active) VALUES (@Total, @Active)
								 SELECT Total, Active FROM @T";

			return new SqlConnection(DA.GetConnectionString(DatabaseNames.web)).QuerySingle<Count>(sql);
		}
	}
}
