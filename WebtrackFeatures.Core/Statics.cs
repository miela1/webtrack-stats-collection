﻿using System.Collections.Generic;

namespace WebtrackFeatures.Core
{
	public static class Statics
	{
		static Dictionary<string, string> functionRefrence;
		public static Dictionary<string, string> FunctionRefrence
		{
			get
			{
				if (functionRefrence == null)
					functionRefrence = StatsFactory.GetFunctionRefrence();
				return functionRefrence;
			}
		}

	}
}
