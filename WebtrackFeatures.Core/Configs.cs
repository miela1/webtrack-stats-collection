﻿using Newtonsoft.Json.Linq;
using System.IO;

namespace WebtrackFeatures.Core
{
	public static class Configs
	{
		static JObject json;
		static JObject Json
		{
			get
			{
				if (json == null)
					json = ConfigHelpers.ReadJson("settings.json");
				return json;
			}
		}
		/// <summary>
		/// Obejct with connection strings 
		/// </summary>
		public static DBConnections DBConnections => new DBConnections(Json["database"]);
	}

	internal static class ConfigHelpers {
		internal static JObject ReadJson(string filename)
		{
			var jsonText = "";
			using (var sr = new StreamReader(Path.Combine(Directory.GetCurrentDirectory(), filename)))
			{
				while (!sr.EndOfStream)
				{
					jsonText += sr.ReadLine();
				}
			};
			return JObject.Parse(jsonText);
		}
	}
}
