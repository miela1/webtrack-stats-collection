﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebtrackFeatures.Core
{
	public static class Helpers
	{
		public static bool IsWebTrack(string url)
		{
			if (url.Contains("web-track.dk"))
				return true;
			return false;
		}

		public static bool IsTest(string url)
		{
			if (url.Contains("test.web-track.dk"))
				return true;
			return false;
		}

		public static bool IsOnline(string url)
		{
			if (url.Contains("/online"))
				return true;
			if (url.Split("/").Length <= 1)
				return true;
			if (url == "https://www.web-track.dk/")
				return true;
			return false;
		}
	}
}
