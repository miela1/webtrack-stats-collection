﻿namespace WebtrackFeatures.Core.Types
{
	public class ClickCount
	{
		public int X { get; set; }
		public int Y { get; set; }
		public int Count { get; set; }
		public int Width { get; set; }
		public int Height { get; set; }
	}
}
