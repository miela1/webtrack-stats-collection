﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace WebtrackFeatures.Core
{
	public class DBConnections
	{
		public string DagState { get; }
		public Dictionary<string, string> ConnectionStrings { get; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="json"></param>
		public DBConnections(JToken json)
		{
			DagState = json["ConnectionState"].ToString();
			foreach (var el in json["ConnectionStrings"])
			{
				if (ConnectionStrings == null)
					ConnectionStrings = new Dictionary<string, string>();

				ConnectionStrings.Add(el["Name"].ToString(), el["ConnectionString"].ToString());
			}
		}
	}
}
