﻿using System.Collections.Generic;

namespace WebtrackFeatures.Models.Functions
{
	public class FunctionsData
	{
		public Dictionary<string, int> FunctionsCount { get; set; }
	}
}
