﻿using System.Collections.Generic;
using WebtrackFeatures.Core.DataItems;

namespace WebtrackFeatures.Models.Default
{
	public class DefaultViewModel
	{
		public Count UserCount { get; set; }
		public Count CustomerCount { get; set; }
		public Count DeviceCount { get; set; }

		public Dictionary<string, int> BrowserCount { get; set; }
		public Dictionary<string, int> OsCount { get; set; }
		public Dictionary<string, int> LanguageCount { get; set; }
		public Dictionary<string, int> LoadTimesDataPoints { get; set; }
		public Dictionary<string, int> DeviceTypeCount { get; set; }


		public Dictionary<string, double> LoadTimes { get; set; }

		public Dictionary<string, int> UserAgents { get; set; }

		public int Visits { get; set; }

	}
}
