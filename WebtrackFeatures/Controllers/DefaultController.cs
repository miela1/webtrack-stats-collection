﻿using MessagePack;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using r2pCore.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using WebtrackFeatures.Core;
using WebtrackFeatures.Core.DataItems;
using WebtrackFeatures.Models.Default;

namespace WebtrackFeatures.Controllers
{
	public class DefaultController : Controller
	{
		IMemoryCache _cache;
		public DefaultController(IMemoryCache cache)
		{
			_cache = cache;
		}
		public IActionResult Index()
		{

			var data = new DefaultViewModel
			{
				UserCount = UserFactory.GetUserCount(),
				CustomerCount = CustomerFactory.GetCustomersCount(),
				DeviceCount = DevciceFactory.GetDeviceCount(),
				Visits = CalcVisits(),
				OsCount = new Dictionary<string, int>(),
				LanguageCount = new Dictionary<string, int>(),
				LoadTimes = new Dictionary<string, double>(),
				LoadTimesDataPoints = new Dictionary<string, int>(),
				BrowserCount = new Dictionary<string, int>(),
				DeviceTypeCount = new Dictionary<string, int>(),
				UserAgents = new Dictionary<string, int>()
			};

			foreach (var entry in StatsFactory.GetActivityLogs(_cache))
			{
				if (entry.MessagePackData == null)
					continue;
				if (entry.Action != Actions.PageLoad)
					continue;

				var item = MessagePackSerializer.Deserialize<ActivityLogPackData>(entry.MessagePackData);

				data.LanguageCount.TryGetValue(ToLanguage(item.BrowserInfo.Language), out int lValue);
				data.LanguageCount.AddOrUpdate(ToLanguage(item.BrowserInfo.Language), lValue + 1);
				data.UserAgents.TryGetValue(item.BrowserInfo.UserAgent, out int uaValue);
				data.UserAgents.AddOrUpdate(item.BrowserInfo.UserAgent, uaValue + 1);
			}
			var loadTimes = CalcLoadTimes();
			data.LoadTimes = loadTimes.Times;
			data.LoadTimesDataPoints = loadTimes.DataPoints;


			return View("Index", data);
		}

		int CalcVisits()
		{
			var to = DateTime.Now;
			var from = to.AddDays(-1);
			var data = StatsFactory.GetActivityLogs(from, to);

			return data.Count(c => c.MessagePackData != null && c.Action == Actions.PageLoad && Helpers.IsOnline(c.Url));
		}

		LoadTimes CalcLoadTimes()
		{
			var data = StatsFactory.GetActivityLogs(_cache).ToDictionaryList(c => c.UserId);

			var result = new LoadTimes
			{
				Times = new Dictionary<string, double>(),
				DataPoints = new Dictionary<string, int>()
			};

			var tmpTimes = new Dictionary<string, List<double>>();
			foreach (var key in data.Keys)
			{
				DateTime lastTime = DateTime.Now;
				var lastAction = Actions.PageLoad;
				foreach (var item in data.GetValue(key).OrderBy(x => x.Timestamp))
				{
					var test = false;
					if (Helpers.IsTest(item.Url))
						test = true;
					if (!Helpers.IsWebTrack(item.Url))
						continue;
					if (item.Action != Actions.PageLoad && item.Action != Actions.PageLeave)
						continue;

					var url = item.Url.Split("?")[0];
					url = url.Split("#")[0];
					url = url.Substring(url.LastIndexOf('/'));
					if (Helpers.IsOnline(item.Url))
						url = "/online";
					if (Helpers.IsTest(item.Url))
						url = "test " + url;

					if (item.Action == lastAction)
						continue;

					if (lastAction == Actions.PageLeave)
					{
						if (!tmpTimes.ContainsKey(url))
							tmpTimes.Add(url, new List<double>());

						var dif = item.Timestamp - lastTime;
						var difMS = dif.TotalMilliseconds;
						if (difMS <= 5000)
							tmpTimes[url].Add(difMS);
					}

					lastAction = item.Action;
					lastTime = item.Timestamp;
				}
			}

			foreach (var key in tmpTimes.Keys)
			{

				var count = tmpTimes[key].Count;
				var sum = 0D;

				tmpTimes[key].ForEach(c => sum += c);
				if (count > 0)
				{

					result.DataPoints.Add(key, count);
					result.Times.Add(key, Math.Floor(sum / count));
				}
			}



			return result;
		}

		string ToLanguage(string code)
		{
			return (code.Split('-')[0]) switch
			{
				"da" => "Danish",
				"nn" => "Norwigian",
				"no" => "Norwigian",
				"nb" => "Norwigian",
				"en" => "English",
				"pt" => "Portuguese",
				"de" => "German",
				"sv" => "Swedish",
				"es" => "Spanish",
				"vi" => "Vietnamese",
				"nl" => "Dutch",
				"pl" => "Polish",
				"cs" => "Czech",
				"tr" => "Turkish",
				"it" => "Italian",
				"fr" => "French",
				"fo" => "Faroese",
				"lt" => "Lithuanian",
				"am" => "Amharic",
				"ro" => "Romanian",
				_ => "unknown (" + code + ")",
			};
		}
	}
	public class LoadTimes
	{
		public Dictionary<string, double> Times { get; set; }
		public Dictionary<string, int> DataPoints { get; set; }
	}
}