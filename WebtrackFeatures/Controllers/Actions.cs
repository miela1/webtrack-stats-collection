﻿namespace WebtrackFeatures.Controllers
{
	public static class Actions
	{
		public static string PageLoad { get; } = "PAGELOAD";
		public static string PageLeave { get; } = "PAGELEAVE";
		public static string MouseClick { get; } = "MOUSECLICK";
	}
}