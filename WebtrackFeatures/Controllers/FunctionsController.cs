﻿using MessagePack;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using WebtrackFeatures.Core;
using WebtrackFeatures.Core.DataItems;
using WebtrackFeatures.Models.Functions;
using r2pCore.Common.Extensions;

namespace WebtrackFeatures.Controllers
{
	public class FunctionsController : Controller
	{

		IMemoryCache _cache;
		public FunctionsController(IMemoryCache cache)
		{
			_cache = cache;
		}

		public IActionResult Index()
		{
			var data = new FunctionsData { };

			return View(data);
		}

		public JsonResult Heatmap(string id)
		{
			var logs = StatsFactory.GetActivityLogs(_cache);
			var clicks = new List<Position>();

			foreach (var entry in logs)
			{
				if (entry.MessagePackData == null)
					continue;
				if (!entry.Url.Contains(id))
					continue;
				if (entry.Action != Actions.MouseClick)
					continue;
				try
				{
					var packData = MessagePackSerializer.Deserialize<ActivityLogPackData>(entry.MessagePackData);
					clicks.Add(new Position { X = packData.MousePosition.X, Y = packData.MousePosition.Y, Height = packData.WindowSize.Height, Width = packData.WindowSize.Width });
				}
				catch { }
			}
			return Json(clicks);
		}

		public JsonResult FunctionCount(string id)
		{
			var logs = StatsFactory.GetActivityLogs(_cache);
			var names = StatsFactory.GetFunctionNames(_cache);

			var functions = new Dictionary<string, int>();
			foreach (var entry in logs)
			{
				if (entry.MessagePackData == null)
					continue;
				if (!entry.Url.Contains(id))
					continue;
				if (entry.Action != Actions.MouseClick)
					continue;
				try
				{
					var packData = MessagePackSerializer.Deserialize<ActivityLogPackData>(entry.MessagePackData);
					var key = names.Find(x => x.Selector == packData.TargetSelect).Name;
					if (!key.HasValue())
						key = packData.TargetSelect;

					if (Helpers.IsTest(entry.Url))
						key = "Test - " + key;

					if (!functions.ContainsKey(key))
						functions.Add(key, 0);
					functions[key] += 1;
				}
				catch { }
			}

			return Json(functions);
		}
	}
}