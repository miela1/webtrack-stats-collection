﻿class Screenshot {
	get Width() { return window.innerWidth; }
	get Height() { return window.innerHeight; }
	constructor() {
		this.Canvas = document.createElement("canvas");
		this.StreamPlayer = document.createElement("video");

		this.Canvas.width = this.Width;
		this.Canvas.height = this.Height;
	};

	ClearCanvas() {
		var ctx = this.Canvas.getContext("2d");
		ctx.fillStyle = "#AAA";
		ctx.fillRect(0, 0, this.Width, this.Height);
	}

	async StartCapture() {
		var mediaStream = await navigator.mediaDevices.getDisplayMedia({ video: true, audio: false });

		console.log(mediaStream);

		this.StreamPlayer.srcObject = mediaStream;
		this.StreamPlayer.play();

		setTimeout(() => {
			this.Take();
		}, 1000);
	}
	StopCapture() {
		return;
	}

	Take() {
		var ctx = this.Canvas.getContext("2d");
		ctx.drawImage(this.StreamPlayer, 0, 0, this.Width, this.Height);

		this.SaveImage();
		this.Clean();
	}

	Clean() {
		this.StopCapture();
		this.tmpPlayer = null;
		this.Canvas = null;
	}

	SaveImage() {
		var imageData = this.Canvas.toDataURL("image/png");
		var link = document.createElement("a");
		var date = new Date();
		link.download = `r2p_screen_capture-${document.title}-${date.toISOString()}.png`;
		link.href = imageData;
		link.click();
	}
}