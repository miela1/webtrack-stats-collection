﻿class Heatmap {
	/**
	 * @constructor
	 * @example
	 * var map = new Heatmap("#tachodownload_heatmap", {
	 *		mapSize: {
	 *			height: 600,
	 *			width: HeatmapDefaults.Parent
	 *		},
	 *		dataUrl: "./functions/heatmap/tachodownload",
	 *		backgroundImage: "./assets/images/tachodownload.png",
	 *		downloadButton: "#tachodownload-download"
	 * });
	 * map.Draw();
	 * 
	 * @param {HtmlCanvasElement} canvas
	 * @param {HeatmapOptions} opts
	 */
	constructor(canvas, opts) {
		if (typeof canvas === "string")
			canvas = dom.find(canvas, true);
		this.Canvas = canvas;
		this.Options = HeatmapDefaults.Options;
		if (opts)
			Object.assign(this.Options, opts);

		this.ParentElement = this.Canvas.parentElement;
		if (this.Options.mapSize.width === HeatmapDefaults.Parent) {
			let comp = getComputedStyle(this.ParentElement);
			let padding = parseInt(comp.paddingLeft) + parseInt(comp.paddingRight);

			this.Options.mapSize.width = this.ParentElement.clientWidth - padding;
		}
		if (this.Options.mapSize.height === HeatmapDefaults.Parent) {
			let comp = getComputedStyle(this.ParentElement);
			let padding = parseInt(comp.paddingBottom) + parseInt(comp.paddingTop);

			this.Options.mapSize.height = this.ParentElement.clientHeight - padding;
		}

		this.Canvas.height = this.Options.mapSize.height;
		this.Canvas.width = this.Options.mapSize.width;

		if (this.Options.downloadButton != null) {
			if (typeof this.Options.downloadButton === "string")
				this.Options.downloadButton = dom.find(this.Options.downloadButton, true);

			this.Options.downloadButton.addEventListener("click", ev => {
				ev.preventDefault();
				this.MakeDownload();
			})
		}
	}
	/**
	 * Draws the heatmap
	 * @async
	 */
	async Draw() {
		var ctx = this.Canvas.getContext("2d");

		await this.DrawBackground(ctx);
		if (this.Options.showSectors)
			this.DrawSectors(ctx, 20);

		if (this.Options.dataUrl && !this.Options.data) {
			await this.LoadAjaxData(this.Options.dataUrl);
		}
		if (this.Options.data && !this.Options.dataUrl) {
			this.LoadData(this.Options.data, ctx);
		}

	}
	/**
	 * @private
	 * @param {CanvasRenderingContext2D} ctx
	 * @param {number} count the number of sectorss to be drawn
	 */
	DrawSectors(ctx, size) {
		var countX = Math.floor(this.Options.mapSize.width / size);
		var countY = Math.floor(this.Options.mapSize.height / size);

		ctx.fillStyle = "#FF6347"
		ctx.strokeStyle = "#f39100"
		for (var i = 0; i <= countX; i++) {
			var x = size * i;
			if (i !== 0) {
				ctx.font = "10px serif";
				ctx.fillText(i, x + 2, 10);
			}

			//Line X
			ctx.beginPath();
			ctx.moveTo(x, 0);
			ctx.lineTo(x, this.Options.mapSize.height);
			ctx.stroke();

		}
		for (var i = 0; i <= countY; i++) {
			var y = size * i;
			if (i !== 0) {
				ctx.font = "10px serif";
				ctx.fillText(i, 5, y - 6);
			}

			//Line y
			ctx.beginPath();
			ctx.moveTo(0, y);
			ctx.lineTo(this.Options.mapSize.width, y);
			ctx.stroke();
		}
	}
	/**
	 * @async
	 * @private
	 * @param {CanvasRenderingContext2D} ctx
	 */
	async DrawBackground(ctx) {
		var background = await this.LoadImage(this.Options.backgroundImage);
		ctx.drawImage(background, 0, 0, this.Options.mapSize.width, this.Options.mapSize.height);
	}
	/**
	 * @private
	 * @param {HeatmapData} items Json data
	 * @param {CanvasRenderingContext2D} ctx
	 */
	DrawHeatItems(items, ctx) {
		if (!ctx)
			ctx = this.Canvas.getContext("2d");

		var positions = HeatmapHelpers.calcPositions(this.Options.mapSize, items);
		var highLow = HeatmapHelpers.FindHighLowValue(positions);

		for (var index in positions) {
			var item = positions[index];

			var radius = HeatmapHelpers.GetRadius(item.count, highLow.high, highLow.low);
			var colour = HeatmapHelpers.GetColour(item.count, highLow.high, highLow.low);


			if (item.count <= this.Options.cutoff)
				continue;

			ctx.fillStyle = colour;
			ctx.beginPath();
			ctx.arc(item.x, item.y, radius, 0, 2 * Math.PI);
			ctx.fill();

		}
		if (this.Options.showScale)
			this.DrawScale(this.ScaleData, highLow.low, highLow.high);
	}

	DrawScale(ctx, min, max) {
		if (!ctx)
			ctx = this.Canvas.getContext("2d");

		var paddingTop = 60;
		var paddingButtom = 30;
		var paddingTB = paddingTop + paddingButtom; //MAX 100px before it starts to look wierd
		var paddingRight = 45;

		var totHight = this.Options.mapSize.height - paddingTB;
		var height = totHight / 4;
		var width = 10;

		var xPos = this.Options.mapSize.width - width - paddingRight;
		var yPos = paddingTop;

		
		var redGradient = ctx.createLinearGradient(0, height, 0, 0);
		var orangeGradient = ctx.createLinearGradient(0, height, 0, 0);
		var yellowGradient = ctx.createLinearGradient(0, height, 0, 0);
		var greenGradient = ctx.createLinearGradient(0, height, 0, 0);

		ctx.font = "10px serif";

		ctx.fillStyle = "#fff";
		ctx.fillRect(xPos, yPos - 12, 30, 12);

		ctx.fillStyle = "#fff";
		ctx.fillRect(xPos, yPos + totHight, 30, 12);

		ctx.fillStyle = "#db1364b6";
		ctx.fillRect(xPos, yPos -12, 30, 12);

		ctx.fillStyle = "#43b01a60";
		ctx.fillRect(xPos, yPos + totHight, 30, 12);



		ctx.fillStyle = "#fff";
		ctx.fillRect(xPos, yPos, width, totHight);

		ctx.fillStyle = "#fff";
		ctx.fillRect(xPos, yPos, width, totHight);

		redGradient.addColorStop(1, HeatmapHelpers.GetColour(20, 20, 0));
		redGradient.addColorStop(0.75, HeatmapHelpers.GetColour(19, 20, 0));
		redGradient.addColorStop(0.50, HeatmapHelpers.GetColour(18, 20, 0));
		redGradient.addColorStop(0.25, HeatmapHelpers.GetColour(17, 20, 0));
		redGradient.addColorStop(0, HeatmapHelpers.GetColour(16, 20, 0));
		ctx.fillStyle = redGradient;
		ctx.fillRect(xPos, yPos, width, height);
		ctx.fillStyle = "#fff";
		ctx.fillText(max, xPos + 1, yPos -2);

		orangeGradient.addColorStop(1, HeatmapHelpers.GetColour(15, 20, 0));
		orangeGradient.addColorStop(0.75, HeatmapHelpers.GetColour(14, 20, 0));
		orangeGradient.addColorStop(0.50, HeatmapHelpers.GetColour(13, 20, 0));
		orangeGradient.addColorStop(0.25, HeatmapHelpers.GetColour(12, 20, 0));
		orangeGradient.addColorStop(0, HeatmapHelpers.GetColour(11, 20, 0));
		ctx.fillStyle = orangeGradient;
		ctx.fillRect(xPos, yPos + height, width, height);



		yellowGradient.addColorStop(1, HeatmapHelpers.GetColour(10, 20, 0));
		yellowGradient.addColorStop(0.75, HeatmapHelpers.GetColour(9, 20, 0));
		yellowGradient.addColorStop(0.50, HeatmapHelpers.GetColour(8, 20, 0));
		yellowGradient.addColorStop(0.25, HeatmapHelpers.GetColour(7, 20, 0));
		yellowGradient.addColorStop(0, HeatmapHelpers.GetColour(6, 20, 0));
		ctx.fillStyle = yellowGradient;
		ctx.fillRect(xPos, yPos + (2 * height), width, height);


		greenGradient.addColorStop(1, HeatmapHelpers.GetColour(5, 20, 0));
		greenGradient.addColorStop(0.75, HeatmapHelpers.GetColour(4, 20, 0));
		greenGradient.addColorStop(0.50, HeatmapHelpers.GetColour(3, 20, 0));
		greenGradient.addColorStop(0.25, HeatmapHelpers.GetColour(2, 20, 0));
		greenGradient.addColorStop(0, HeatmapHelpers.GetColour(1, 20, 0));
		ctx.fillStyle = greenGradient;
		ctx.fillRect(xPos, yPos + (3 * height), width, height);
		ctx.fillStyle = "#000";
		ctx.fillText(min, xPos + 2, yPos + totHight +10);


	}

	/**
	 * @private
	 * @param {string} src
	 */
	LoadImage(src) {
		return new Promise((resolve, reject) => {
			try {
				let img = new Image();
				img.addEventListener("load", () => {
					resolve(img);
				});
				img.addEventListener("error", (ev) => {
					ev.preventDefault();
					console.error("image not found, loading default image");
					this.LoadImage(HeatmapDefaults.Options.backgroundImage).then(i => resolve(i));

				});
				img.src = src;
			} catch (ex) {
				reject(ex);
			}
		});
	}

	/**
	 * @example
	 * var map = new Heatmap("#canvas", { backgroundImage = "someimage.png" });
	 * map.LoadData([{ x:5, y:5, width:1290, heigh:323 }]);
	 * map.Draw().then(e=> console.log("map loaded"));
	 * 
	 * @param {HeatmapData} data data to be loaded to the heatmap in json format 
	 * @param {CanvasRenderingContext2D} [ctx]
	 */
	LoadData(data, ctx) {
		if (typeof data !== "object")
			throw Error(`Data is type of ${typeof data} expectet array`);
		//Validates data
		for (var index in data) {
			var item = data[index];
			if (typeof item.x === "undefined" || typeof item.y === "undefined") {
				throw Error(`Data at index ${index} not in expectet format getting ${JSON.stringify(item)}`);
			}
		}
		this.DrawHeatItems(data, ctx)
	}

	/**
	 * @example
	 * var map = new Heatmap("#canvas", { backgroundImage = "someimage.png" });
	 * map.LoadAjaxData("http://mysite.dk/data");
	 * map.Draw().then(e=> console.log("map loaded"));
	 * 
	 * @param {string} url The url to the Ajax method where the data is stored
	 * @param {string} [rootNode] if the data object has other elerments the the ones for heatmap
	 * @returns {object} Returns loaded data if is needed other places
	 */
	async LoadAjaxData(url, rootNode) {
		var response = await fetch(url);
		if (response.ok) {
			var data = await response.json();

			if (rootNode)
				this.LoadData(data[rootNode]);
			else
				this.LoadData(data);
			return data;
		}
		throw Error("Ajax failed", response.statusText);

	}
	/**
	 * Starts a download of the map as a PNG image
	 */
	MakeDownload() {
		var imageData = this.Canvas.toDataURL("image/png");
		var link = document.createElement("a");
		var date = new Date();
		link.download = `${this.Canvas.id}-${date.toISOString()}.png`;
		link.href = imageData;
		link.click();

	}
}
/**
 * Default values used by Heatmap
 * @hideconstructor
 * @property {HeatmapOptions} Options The default options for the heatmap
 * @property {string} Parent the string "parent"
 */
class HeatmapDefaults {

	/**
	 * @member {HeatmapOptions}
	 */
	static get Options() {
		return {
			backgroundImage: "./assets/images/heatmap-default-background.png",
			mapSize: {
				width: this.Parent,
				height: this.Parent,
			},
			showSectors: false,
			dataUrl: null,
			downloadButton: null,
			cutoff: 200,
			showScale: true
		}
	}
	/**
	 * allways return the string parent
	 * @member {string}
	 */
	static get Parent() {
		return "parent";
	}
}
/**
 * Helper methods used by Heatmap 
 * @static
 * @hideconstructor
 */
class HeatmapHelpers {
	/**
	 * returns colour calculated from count value
	 * @static
	 * @param {number} val
	 * @returns {string} the colour in hex css colour
	 */
	static GetColour(val, max, min) {
		var colours = ["#43b01a", "#fecc00", "#f39100", "#db1364"];

		var middle = (max + min) / 2;
		var betweenMiddleAndMax = (middle + max) / 2;//between middle and max
		var betweenMiddleAndMin = (middle + min) / 2;

		var colour = colours[3] + HeatmapHelpers.GetColourTransparent(val, max, betweenMiddleAndMax);
		if (val <= betweenMiddleAndMax)
			colour = colours[2] + HeatmapHelpers.GetColourTransparent(val, betweenMiddleAndMax, middle);
		if (val <= middle)
			colour = colours[1] + HeatmapHelpers.GetColourTransparent(val, middle, betweenMiddleAndMin);
		if (val <= betweenMiddleAndMin) //between half and min
			colour = colours[0] + HeatmapHelpers.GetColourTransparent(val, betweenMiddleAndMin, min);
		if (val <= min) //min
			colour = colours[0] + HeatmapHelpers.GetColourTransparent(val, min, 0);

		return colour;
	}

	static GetColourTransparent(val, max, min) {
		var middle = (max + min) / 2;
		var betweenMiddleAndMax = (middle + max) / 2;
		var betweenMiddleAndMin = (middle + min) / 2;

		var trans = 192;
		if (val <= betweenMiddleAndMax)
			trans = 160;
		if (val <= middle)
			trans = 128;
		if (val <= betweenMiddleAndMin)
			trans = 96;
		if (val <= min)
			trans = 64;
		return trans.toString(16);
	}

	/**
	 * returns calculated radius from count value
	 * @static
	 * @param {number} val
	 */
	static GetRadius(val, max, min) {
		var middle = (max + min) / 2;
		var betweenMiddleAndMax = (middle + max) / 2;//between middle and max
		var betweenMiddleAndMin = (middle + min) / 2;

		var result = 15;
		if (val <= betweenMiddleAndMax)
			result = 13.75;
		if (val <= middle)
			result = 12.5;
		if (val <= betweenMiddleAndMin)
			result = 11.25;
		if (val <= min)
			result = 10;

		return result;
	}
	/**
	 * Calcualtes the pixel position om the canvas
	 * @static
	 * @param {size} localSize
	 * @param {HeatmapData} data
	 */
	static calcPositions(localSize, data) {
		var result = [];
		for (var index in data) {
			var item = data[index];
			var cX = calc(localSize.width, item.width, item.x);
			var cY = calc(localSize.height, item.height, item.y);
			var buf = 15;
			if (cX == 0 && cY == 0)
				continue;

			var match = result.find(r => {
				if ((r.x > cX - buf && r.x < cX + buf) && (r.y > cY - buf && r.y < cY + buf))
					return true;
				return false;
			});
			if (match) {
				match.count++;
				continue;
			}
			result.push({
				x: cX,
				y: cY,
				count: 1
			});
		}

		function calc(local, source, position) {
			var diffSize = local / source;
			var sourcePos = (source / 100) * position;
			var localPos = (sourcePos * diffSize);
			return Math.floor(localPos)
		}
		return result;
	}
	static FindHighLowValue(positions) {
		var high = 0;
		var low = 9999;
		for (var i in positions) {
			var val = positions[i].count;
			if (val > high)
				high = val;
			if (val < low)
				low = val;
		}
		if (low <= 20)
			low = 20;
		return { high, low };
	}
}


/**
 * @interface HeatmapData
 * @property {number} x The x cordinate in percent
 * @property {number} y The y cordinate in percent
 * @property {number} width The Width of the source elemtent
 * @property {number} height The height of the cource element
 */

/**
 * @interface HeatmapOptions
 * @property {string} backgroundImage The uri to the image used as background
 * @property {Size} mapSize The size of the map
 * @property {boolean} showSectors
 * @property {string} dataUrl The url to the data ajac method
 * @property {number} downloadButton The id of the hetml element used for download a png image
 */

/**
 * @interface Size
 * @property {number} width
 * @property {number} height
 */