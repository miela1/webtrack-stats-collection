﻿class r2pDom {
	constructor(instanceName) {
		if (instanceName) {
			window["dom"] = null;
			window[instanceName] = this;
		}
	}
	/**
	 * 
	 * 
	 * @param {string} selector css style selector to find elements
	 * @param {boolean} single indecates if only the first result is returens
	 * @returns {r2pDomElement}
	 */
	find(selector, single) {
		var elements = document.querySelectorAll(selector);
		if (single)
			return elements[0];
		return elements;
	}

	/**
	 * to prevent r2pdom from autostart, this is used if the instancenames needs to be other then dom or if you want to used a local instance.
	 * */
	static preventAutoStart() {
		r2pDom.preventStart = true;
	}
	/**
	 * Default start call is called automaticly
	 * */
	static Default() {
		if (!this.preventStart)
			new r2pDom("dom");
	}

}


r2pDom.Default();