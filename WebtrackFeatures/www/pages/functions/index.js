﻿var heatmaps = [
	new Heatmap("#online_heatmap", {
		mapSize: {
			height: 600,
			width: HeatmapDefaults.Parent
		},
		dataUrl: "./functions/heatmap/online",
		backgroundImage: "./assets/images/online.png",
		downloadButton: "#online-download",
	}),
	new Heatmap("#history_heatmap", {
		mapSize: {
			height: 600,
			width: HeatmapDefaults.Parent
		},
		dataUrl: "./functions/heatmap/history",
		backgroundImage: "./assets/images/history.png",
		downloadButton: "#history-download",

	}),
	new Heatmap("#reports_heatmap", {
		mapSize: {
			height: 600,
			width: HeatmapDefaults.Parent
		},
		dataUrl: "./functions/heatmap/reports",
		backgroundImage: "./assets/images/reports.png",
		downloadButton: "#reports-download"
	}),

	new Heatmap("#poi_heatmap", {
		mapSize: {
			height: 600,
			width: HeatmapDefaults.Parent
		},
		dataUrl: "./functions/heatmap/poi",
		backgroundImage: "./assets/images/poi.png",
		downloadButton: "#poi-download",
		cutoff:8
	}),
	new Heatmap("#alerts_heatmap", {
		mapSize: {
			height: 600,
			width: HeatmapDefaults.Parent
		},
		dataUrl: "./functions/heatmap/alerts",
		backgroundImage: "./assets/images/alerts.png",
		downloadButton: "#alerts-download",
		cutoff: 10
	}),
	new Heatmap("#messages_heatmap", {
		mapSize: {
			height: 600,
			width: HeatmapDefaults.Parent
		},
		dataUrl: "./functions/heatmap/messages",
		backgroundImage: "./assets/images/messages.png",
		downloadButton: "#messages-download",
		cutoff:10
	}),
	new Heatmap("#tachodownload_heatmap", {
		mapSize: {
			height: 600,
			width: HeatmapDefaults.Parent
		},
		dataUrl: "./functions/heatmap/tachodownload",
		backgroundImage: "./assets/images/tachodownload.png",
		downloadButton: "#tachodownload-download",
		cutoff:5
	})
];
heatmaps.forEach(c => c.Draw());


document.getElementById("popContainer").addEventListener("click", e => document.getElementById("popContainer").style.display = "none");
var functionButtons = document.getElementsByClassName("btn-functions");
for (var index in functionButtons) {
	if (typeof functionButtons[index] != "object")
		continue;

	var btn = functionButtons[index];
	btn.addEventListener("click", showPop);
}

async function showPop(ev) {

	var id = ev.target.id;
	var name = id.substring(0, id.indexOf("-"));
	console.log(name);


	var responce = await fetch(`./functions/functioncount/${name}`);
	var data = await responce.json();

	var table = document.getElementById("popTable");

	var arrHtml = [];
	arrHtml.push(`<thead><tr><th scope="col">Function</th><th scope="col">Clicks</th></tr></thead>`);
	arrHtml.push("<tbody>");
	for (var key in data) {
		arrHtml.push(`<tr scope="row"><td>${key}</td><td>${data[key]}</td></tr>`);
	}
	arrHtml.push("</tbody>");
	table.innerHTML = arrHtml.join("");
	document.getElementById("popContainer").style.display = "block";

	var downloadLink = document.getElementById("popDownload");
	downloadLink.download = `webtrack_functions_${name}_online.xls`; 
	downloadLink.href = "data:application/vnd.ms-excel;base64," + btoa(table.outerHTML);
}