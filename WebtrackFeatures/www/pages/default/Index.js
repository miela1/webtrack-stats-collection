﻿class Charts {
    constructor() { }

    GeneratePieChart(canvasId, data) {
        let ctx = document.getElementById(canvasId).getContext('2d');
        this.UserChart = new Chart(ctx, {
            type: 'pie',
            data: {
                datasets: [{
                    data: [
                        data.active,
                        data.total - data.active
                    ],
                    borderColor: [
                        "rgb(255,255,255)",
                        "rgb(255,255,255)",
                    ],
                    backgroundColor: [
                        "rgb(0,255,0)",
                        "rgb(255,0,0)",
                    ]
                }],
                labels: [
                    'Active',
                    'Deleted'
                ],
            },
            options: Chart.defaults.pie
        });
    }
    GenerateDoughnutChart(canvasId, data) {
        var labels = [];
        var values = [];
        var colours = [];

        for (var i in data) {
            labels.push(i);
            values.push(data[i]);
            colours.push(Helpers.GetColour(colours));
        }
        let ctx = document.getElementById(canvasId).getContext('2d');
        this.UserChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: values,
                    backgroundColor: colours
                }],
                labels: labels,
            },
            options: Chart.defaults.doughnut
        });
    }
    GenerateBarChart(canvasId, data) {
        var live = {
            labels: [],
            values: []
        }
        var test = {
            labels: [],
            values: []
        }
        var colours = [];

        for (var i in data) {
            let container = live;
            if (/test/i.test(i)) {
                container = test;
            }
            container.labels.push(i);
            container.values.push(data[i]);

            colours.push(Helpers.GetColour(colours));
        }

        console.log("live", live);
        console.log("test", test);

        let ctx = document.getElementById(canvasId).getContext('2d');
        this.UserChart = new Chart(ctx, {
            type: 'bar',
            data: {
                datasets: [{
                    data: live.values,
                    backgroundColor: "green",
                    labels: live.labels,
                    label: 'live'
                },
                {
                    data: test.values,
                    backgroundColor: "yellow",
                    labels: test.labels,
                    label: 'test'
                }
                ],
                labels: live.labels
            },
            options: Chart.defaults.bar
        });
    }


    UpdateUserAgent(uaArray) {
        var parser = new UAParser();

        var uaBrowser = {},
            uaOs = {},
            uaDeviceType = {};

        for (let i in uaArray) {
            parser.setUA(i);
            let uaObject = parser.getResult();

            if (!uaBrowser[uaObject.browser.name])
                uaBrowser[uaObject.browser.name] = 0;
            uaBrowser[uaObject.browser.name] += uaArray[i];

            if (!uaOs[`${uaObject.os.name}`])
                uaOs[`${uaObject.os.name}`] = 0;
            uaOs[`${uaObject.os.name}`] += uaArray[i];


            let typeName = "Computer";
            if (uaObject.device.type)
                typeName = uaObject.device.type;

            if (!uaDeviceType[typeName])
                uaDeviceType[typeName] = 0;
            uaDeviceType[typeName] += uaArray[i];
            if (!_model.DataCount)
                _model.DataCount = 0;
            _model.DataCount += uaArray[i];
        }
        _model.browserCount = uaBrowser;
        _model.osCount = uaOs;
        _model.deviceTypeCount = uaDeviceType;
    }

    LoadCharts() {

        this.GeneratePieChart("device-chart", _model.deviceCount);
        this.GeneratePieChart("users-chart", _model.userCount);
        this.GeneratePieChart("customer-chart", _model.customerCount);

        this.GenerateDoughnutChart("browser-chart", _model.browserCount);
        this.GenerateDoughnutChart("os-chart", _model.osCount);
        this.GenerateDoughnutChart("language-chart", _model.languageCount);
        this.GenerateDoughnutChart("client-type-chart", _model.deviceTypeCount);

        this.GenerateBarChart("avgload-chart", _model.loadTimes);
        this.GenerateBarChart("avgload-datapoints-chart", _model.loadTimesDataPoints);



    }

    RunPage() {
        this.UpdateUserAgent(_model.userAgents);
        this.LoadCharts();
    }
}

new Charts().RunPage();
